FROM archlinux:latest

LABEL maintainer="Nicolas Schuler"
LABEL web="n-schuler.dev"
LABEL email="mail@n-schuler.dev"
LABEL version="0.1"
LABEL description="Arch-based texlive docker image with pygments support and a few available fonts"

# Biber Path
ENV PATH="/usr/bin/vendor_perl:${PATH}"

RUN pacman -Syw && \
    pacman -Syu --noconfirm && \
    pacman --noconfirm -S \
        which \
        wget \
        git \
        pandoc \
        inkscape \
        python \
        python-pip \
        biber \
        ghostscript \
        t1utils \
        texlive-most \
        texlive-lang \
        perl-clone && \
    pip install Pygments && \
    pacman -S --noconfirm \
         adobe-source-code-pro-fonts \
         adobe-source-sans-pro-fonts \
         adobe-source-serif-pro-fonts \
         gnu-free-fonts \
         gsfonts \
         inter-font \
         libfontenc \
         libotf \
         noto-fonts-emoji \
         noto-fonts-extra \
         ttf-fira-code \
         ttf-fira-mono \
         ttf-fira-sans \
         ttf-font-awesome \
         ttf-opensans \
         ttf-roboto \
         ttf-roboto-mono \
         ttf-ubuntu-font-family \
         ttf-dejavu \
         noto-fonts \
         powerline-fonts \
         ttc-iosevka \
         ttf-hack && \
    pacman -Scc --noconfirm

